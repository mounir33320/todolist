<?php

namespace App\Controller;

use App\Entity\Task;
use App\Entity\TaskList;
use App\Form\TaskType;
use App\Service\TaskService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class TaskController extends AbstractController
{
    public function __construct(private TaskService $taskService){}

    #[Route("/taskList/{id}/task/create", name: "task_create")]
    public function create(Request $request, TaskList $taskList): RedirectResponse|Response
    {
        $task = new Task();
        $form = $this->createForm(TaskType::class, $task);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $task->setTaskList($taskList);
            $this->taskService->save($task);
            $this->addFlash("success", "Cette tâche a bien été ajoutée à la liste");

            return $this->redirectToRoute("task_list_show", ["id" => $taskList->getId()]);
        }

        return $this->render("task/create.html.twig", [
            "form" => $form->createView()
        ]);
    }

    #[Route("/task/edit/{id}", name: "task_edit")]
    public function edit(Request $request, Task $task): RedirectResponse|Response
    {
        $form = $this->createForm(TaskType::class, $task);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $this->taskService->update($task);
            $this->addFlash("success", "Tâche modifiée avec succès");

            return $this->redirectToRoute("task_list_show", ["id" => $task->getTaskList()->getId()]);
        }

        return $this->render("task/edit.html.twig", [
            "form" => $form->createView(),
            "task" => $task
        ]);
    }

    #[Route("/task/edit/is-finished/{id}", name: "task_edit_is_finished", methods: ["POST"])]
    public function editIsFinished(Request $request, Task $task): JsonResponse
    {
        $data = json_decode($request->getContent(), true);

        if(!isset($data["isFinished"])){
            return new JsonResponse(["success" => false, "error" => "Les données envoyées ne sont pas correctes"], Response::HTTP_BAD_REQUEST);
        }

        $task->setIsFinished($data["isFinished"]);
        $this->taskService->update($task);

        return new JsonResponse(["success" => true], Response::HTTP_OK);
    }

    #[Route("/task/delete/{id}", name: "task_delete")]
    public function delete(Request $request, Task $task): RedirectResponse
    {
        $token = $request->get("_token");

        if($this->isCsrfTokenValid("task-{$task->getId()}", $token)){
            $this->taskService->delete($task);
            $this->addFlash("success", "Tâche supprimée avec succès");

            return $this->redirectToRoute("task_list_show", ["id" => $task->getTaskList()->getId()]);
        }

        $this->addFlash("error", "Une erreur est survenue...");

        return $this->redirectToRoute("task_list_show", ["id" => $task->getTaskList()->getId()]);
    }
}

<?php

namespace App\Controller;

use App\Entity\TaskList;
use App\Form\TaskListType;
use App\Service\TaskListService;
use App\Service\TaskService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route("/", name: "task_list_")]
class TaskListController extends AbstractController
{
    public function __construct(private TaskListService $taskListService)
    {
    }

    #[Route("", name: "index")]
    public function index(): Response
    {
        $allTaskList = $this->taskListService->getAll();

        return $this->render("task_list/index.html.twig", [
            "allTaskList" => $allTaskList
        ]);
    }

    #[Route("/create", name: "create")]
    public function create(Request $request): RedirectResponse|Response
    {
        $taskList = new TaskList();

        $form = $this->createForm(TaskListType::class, $taskList);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $this->taskListService->save($taskList);
            $this->addFlash("success", "Liste créée avec succès");

            return $this->redirectToRoute("task_list_show", ["id" => $taskList->getId()]);
        }

        return $this->render("task_list/create.html.twig", [
            "form" => $form->createView()
        ]);
    }

    #[Route("/{id}", name: "show")]
    public function show(TaskList $taskList, Request $request): Response|RedirectResponse
    {
        $form = $this->createForm(TaskListType::class, $taskList);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $this->taskListService->update($taskList);
            $this->addFlash("success", "Liste modifiée avec succès");

            return $this->redirectToRoute("task_list_show", ["id" => $taskList->getId()]);
        }

        return $this->render("task_list/show.html.twig", [
            "taskList" => $taskList,
            "form" => $form->createView()
        ]);
    }

    #[Route("/delete/{id}", name: "delete")]
    public function delete(Request $request, TaskList $taskList): RedirectResponse
    {
        $token = $request->get("_token");

        if($this->isCsrfTokenValid("taskList-{$taskList->getId()}", $token)){
            $this->taskListService->delete($taskList);

            $this->addFlash("success", "Liste de tâches supprimée avec succès");
            return $this->redirectToRoute("task_list_index");
        }

        $this->addFlash("error", "Une erreur est survenue...");
        return $this->redirectToRoute("task_list_index");
    }

    #[Route("/delete-finished-tasks/{id}", name: "delete_finished_tasks")]
    public function deleteAllFinishedTasks(Request $request, TaskList $taskList, TaskService $taskService): RedirectResponse
    {
        $token = $request->get("_token");

        if($this->isCsrfTokenValid("taskList-{$taskList->getId()}", $token)){
            foreach ($taskList->getTasks() as $task){
                if($task->isFinished()){
                    $taskService->delete($task);
                }
            }

            $this->addFlash("success", "Les tâches ont été supprimées avec succès");
            return $this->redirectToRoute("task_list_show", ["id" => $taskList->getId()]);
        }

        $this->addFlash("error", "Une erreur est survenue...");
        return $this->redirectToRoute("task_list_show", ["id" => $taskList->getId()]);
    }
}

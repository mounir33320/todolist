<?php

namespace App\DataFixtures;

use App\Entity\Task;
use App\Repository\TaskListRepository;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class TaskFixtures extends Fixture implements DependentFixtureInterface
{
    public function __construct(private TaskListRepository $taskListRepository) { }

    public function load(ObjectManager $manager)
    {
        for($i = 1; $i < 50; $i++){
            $randomNumber = rand(1, 10);
            $taskList = $this->taskListRepository->findOneBy(["name" => "Liste $randomNumber"]);
            $task = (new Task())->setTitle("Tâche $i")->setTaskList($taskList)->setIsFinished(rand(0,1));

            $manager->persist($task);
        }

        $manager->flush();
    }

    public function getDependencies(): array
    {
        return [
            TaskListFixtures::class,
        ];
    }
}

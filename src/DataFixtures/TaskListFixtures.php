<?php

namespace App\DataFixtures;

use App\Entity\TaskList;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class TaskListFixtures extends Fixture
{

    public function load(ObjectManager $manager)
    {
        for($i = 1; $i <= 10; $i++){
            $taskList = (new TaskList())->setName("Liste $i");
            $manager->persist($taskList);
        }

        $manager->flush();
    }
}

<?php

namespace App\Service;

use App\Entity\TaskList;
use App\Repository\TaskListRepository;
use Doctrine\ORM\Exception\ORMException;
use Doctrine\ORM\OptimisticLockException;

class TaskListService
{
    public function __construct(private TaskListRepository $taskListRepository)
    {
    }

    public function getAll(): array
    {
        return $this->taskListRepository->findAll();
    }

    public function save(TaskList $taskList)
    {
        $this->taskListRepository->add($taskList, true);
    }

    public function update(TaskList $taskList)
    {
        $this->taskListRepository->update($taskList);
    }

    /**
     * @throws OptimisticLockException
     * @throws ORMException
     */
    public function delete(TaskList $taskList)
    {
        $this->taskListRepository->remove($taskList, true);
    }
}

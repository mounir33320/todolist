<?php

namespace App\Service;

use App\Entity\Task;
use App\Repository\TaskRepository;

class TaskService
{

    public function __construct(private TaskRepository $taskRepository){}

    public function save(Task $task)
    {
        $this->taskRepository->add($task, true);
    }

    public function update(Task $task)
    {
        $this->taskRepository->update($task);
    }

    public function delete(Task $task)
    {
        $this->taskRepository->remove($task, true);
    }
}

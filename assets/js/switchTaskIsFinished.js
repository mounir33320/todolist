import {showToast} from "./toast";

const allSwitches = document.querySelectorAll(".switch-task-is-finished");

allSwitches.forEach(element => {
    element.onchange = async () => {
        const url = element.dataset.url;
        const isFinished = element.checked;

        const response = await fetch(url, { method: "POST", body: JSON.stringify({ isFinished: isFinished}) })
        if(!response.ok){
            const result = await response.json();
            showToast("error", result.error)
        }
        else {
            changeRowBackground(element);
        }
    }
})

const changeRowBackground = (element) => {
    const trElement = element.closest("tr");
    if(element.checked) {
        trElement.classList.replace("bg-warning", "bg-success");
    }
    else {
        trElement.classList.replace("bg-success", "bg-warning");
    }

}

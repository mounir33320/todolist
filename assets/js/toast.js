
import "notyf/notyf.min.css";
import {Notyf} from "notyf";

let toastElts = document.querySelectorAll(".toast-notification");

if(toastElts){
    toastElts.forEach(element => {
        showToast(element.dataset.type, element.dataset.message);
    })
}

export function showToast(type, message){
    const notyf = new Notyf();

    notyf.open({
        type: type,
        message: message,
        position: {
            x: "right",
            y: "top"
        },
        duration: 5000
    })
}
